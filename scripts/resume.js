(function ($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top)
        }, 1000, 'easeInOutCirc');
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function () {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#sideNav'
  });

  // Set the proper text for each icon when clicked
  $('#progLang .list-inline-item i').click(function () {
    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      $('#progLangInfo').hide(
        1000,
        'easeInOutElastic'
      );
    } else {
      var pText = progLangInfo($(this).parent().attr('id'));
      $('#progLang .list-inline-item i').removeClass('active');
      $(this).addClass('active');
      if ($('#progLangInfo').css('display') == 'none') {
        $('#progLangInfo').html(pText);
        $('#progLangInfo').show(
          1000,
          'easeInOutElastic'
        );
      } else {
        $('#progLangInfo').fadeOut(750, function () {
          $('#progLangInfo').html(pText);
          $('#progLangInfo').fadeIn(750);
        });
      }
    }
  });

  $('#devTool .list-inline-item i').click(function() {
    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      $('#devToolInfo').hide(
        1000,
        'easeInOutElastic'
      );
    } else {
      var pText = devToolInfo($(this).parent().attr('id'));
      $('#devTool .list-inline-item i').removeClass('active');
      $(this).addClass('active');
      if ($('#devToolInfo').css('display') == 'none') {
        $('#devToolInfo').html(pText);
        $('#devToolInfo').show(
          1000,
          'easeInOutElastic'
        );
      } else {
        $('#devToolInfo').fadeOut(750, function () {
          $('#devToolInfo').html(pText);
          $('#devToolInfo').fadeIn(750);
        });
      }
    }
  });

  function progLangInfo(iconID) {
    var detailText;
    switch (iconID) {
      case 'html':
        detailText = 'I started learning HTML in seventh grade when I made my first website all about Pokméon. I was obsessed with the &lt;marquee&gt; element and had images and text sliding all over the place. It was quite a mess. Since that time I have used it in everything from a high school pictures page to an embedded wifi μc interface.';
        break;
      case 'css':
        detailText = 'When I first started with web programming back in seventh grade I never knew what the style tags in my html were actually doing or that they were technically a different language. I just knew how to make my text red. I finally found CSS when I needed to add a background image to my cardomain page to show off my awesome 2000 chevy cavalier.';
        break;
      case 'js':
        detailText = 'Javascript is a relatively new skill for me. I was certialy aware that it existed back in the day, but have only recently implemented it in my projects. I first started learning JS by being creative with <a href="http://www.p5js.org" target="_blank">p5js</a> sketches. Now I am changing over some of my initial php code into javascript where appropriate. AJAX for updating pages without reloading is very nice.';
        break;
      case 'jquery':
        detailText = 'I started learning JS with <a href="http://www.p5js.org" target="_blank">p5js</a> and it did not require much DOM manupulation. This site is my first need to use JS for DOM manipulation and hence is my first real exploration of the jQuery library. It makes selecting DOM elements so easy and clean. I am glad to have learned vanilla JS first though as it really helped me understand the somewhat cryptic jQuery syntax.';
        break;
      case 'python':
        detailText = 'I fell into python with linux on the raspberry pi. It was the first way I learned to control the GPIO on a raspberry pi. I would set python programs to run automatically on boot that would take inputs and switch GPIO accordingly. I ended up writing python code on an embedded linux platform to communicate with AWS shadows. Python is my go to calculator now-a-days.';
        break;
      case 'php':
        detailText = 'When my wife and I bought our house we had a housewarming party. She wanted a guestbook for the guests to sign so I wrote a basic website with php running on a raspberry pi for guests to connect to and leave a message or upload a picture. Now it has evolved into a site we use for home automation, has a guestbook with user registration, and a CRUD api to control the backend database.';
        break;
      case 'go':
        detailText = 'In my constant search for programming videos on youtube I stumbled upon a series about Google&#39;s Go. I ended up getting in touch with the creator of the series and took a ten week intro course with about six other people over google hangouts. We started off with the basics like fizzbuzz and worked up to implementing concurrency and websockets. Still looking for a project to practice my Go skills more.';
        break;
      case 'c':
        detailText = 'I wrote my first μc code in Arduino. I wanted to control some RGB LEDs and could not figure out why the high level Arduino functions did not meet the timing requirements for the RGB LED protocol. Before I realized I could use a library I figured I needed a faster μc. I went through all of the MSP430 tutorials and bit banged out a crude WS2812B protocol.';
        break;
      case 'cplusplus':
        detailText = 'The Arduino platform was my introduction to C++ and where the majority of my C++ experience lies. I have used C++ with Arduinos and ESP8266 development boards for all sorts of projects from adding Google Home voice control to my living room fan, to controlling RGB LEDs, to a desk toy project with an OLED screen for a friend.';
        break;
      case 'csharp':
        detailText = 'C# is as close as I come to being a professional coder. While at one of my jobs I designed an optical bench that needed precise alignment, as they often do. In order to help with the alignment process I wrote an application in C# that took the inputs from the system and fed them into an optimization algorithim that was graphed live while the system was aligned.';
        break;
      case 'dotnet':
        detailText = 'I wrote an alignment program for an optical system I designed that made use of the .net framework with WinForms to give the user control over the optimization algorithim as it was graphed in real time. I have also briefly explored the ASP.NET MVC framework, but all of my webservers run linux so I have found it easier to use other languages and frameworks so far. I am looking into running .net core apps on my linux machines.';
        break;
      case 'bootstrap':
        detailText = 'I enjoy being creative and given enough time I would learn enough HTML, CSS, and JS to come up with my own bootstrap framework. In fact I started out this way when I made simple websites in grade and high school. I am always looking for ways to do things more efficiently and bootstrap provides an excellent platform of the basics from which creativity can run wild.';
        break;
      case 'wordpress':
        detailText = 'I usually prefer to do everything myself because I learn better that way. However there are great tools out there, such as Wordpress, that can help get a project up and running quickly. I learned enough php to understand how Wordpress works and then I used Wordpress with various themes to get a site running for a business that two friends and I have been working on.';
        break;
    }
    return detailText;
  }

  function devToolInfo(iconID) {
    var detailText;
    switch (iconID) {
      case 'nginx':
        detailText = 'This website is running on nginx. My other website <a href="http://www.lindsayandbryan.com" target="_blank">Lindsay and Bryan</a> is running on nginx. When I was searching for a webserver I came across two major options. I decided on nginx because it was lightweight and that would help it run smoothly on my raspberry pi. I also seem to follow the nginx URI interpretation a bit better than Apache\'s .htaccess scheme. The owncloud server I maintain runs apache, but I rarely have to deal with the webserver configuration.';
        break;
      case 'mysql':
        detailText = 'The first time I needed to store information on my webserver I was saving individual files and appending to them. This worked fine for a guestbook that 20 people signed at a housewarming party but it was really a silly way of doing things looking back on it. When I graduated to having users create accounts with profiles and passwords I stepped up to a relational database. I went with Maria because I like their mission statement and SQL is so common.';
        break;
      case 'ssh':
        detailText = 'Ssh\'ing into my servers makes me feel like Boris Grishenko from Goldeneye every time. Just saying the words ssh tunneling sounds cool. Since my webservers are run on headless raspberry pi machines tucked away with my router most of my interaction with them is through ssh. I have setup my key pairs and rotate them every so often to keep security tight.';
        break;
      case 'vstudio':
        detailText = 'Visual Studio and Visual Studio Code are my go to IDEs. I really enjoy the fact that full fledged visual studio is available for free via the community edition. I use VS community when developing in C#. For web development in javascript, html, php, css, etc... I turn to VS Code. I had previously used sublime text because of the simplicity, but VS Code nailed it on that front. I also love that it runs on my ubuntu laptop.';
        break;
      case 'git':
        detailText = 'Git is the go to for version control. I built a decent understanding of version control through my experience with CAD implementations like windchill and PDM so getting my head around git wasn\'t too much of a stretch. For a while I was only using git to maintaion my own personal projects, which was great, but I didn\'t truly understand the power until I started contributing to an open source project on github.';
        break;
      case 'bitbucket':
        detailText = 'Bitbucket was how I learned to use git. Originally I wanted a private place to store my personal projects and turned to bitbucket because of the ability to set repositories to private unlike github. Now bitbucket is where I store my own personal projects and when I want to contribute to open source work I go to github. I also like that bitbucket is in the Atlassian family with jira and confluence.';
        break;
      case 'jira':
        detailText = 'I use jira at FLIR to track design tasks and project requirements. It does feel a bit clunky and perhaps not the best tool for tracking mechanical design tasks but we really don\'t go into tremendous detail when entering tasks and only make use of the task titles and sub tasks. We use what we need out of the tool to be efficient and not much else. I imagine the software department gets much more out of it.';
        break;
      case 'confluence':
        detailText = 'I use confluence at FLIR to document my mechanical designs. I have been chosen as the resident confluence expert in my office because when it was first introduced I immediately started using it instead of saving endless powerpoints on endless shared network drives. I wrote a how to for FLIR and am always looking for new macros to help display the info I need to share in the most effective way possible. To me it is the professional wikipedia.';
        break;
      case 'docker':
        detailText = 'My brothers and I enjoy spending time together. Living 2000 miles apart makes that difficult. I run a minecraft server inside of a docker container on one of my linux boxes so we have somewhere virtual to hangout and play while we chat.';
        break;
      case '3dmodel':
        detailText = '<table><tr><td><img src="img/C-W49T4TT2UQ.png" width="74" height="74"></img></td><td><img src="img/C-VSBB2NSQT4.png" width="74" height="74"></img></td><td>I have been using 3d parametric modeling software since before I graduated in 2010. While at UCF I took an elective course to learn PTC Creo, which was called Pro Engineer back then, along with Solidworks. I am proficient in both and have used Autodesk Inventor and Fusion 360, as well. Scan the QR codes to see my CSWA and CSWP credentials for Solidworks.</td></tr></table>';
        break;
      case 'godot':
        detailText = 'I have always had an interest in video games. Recently my brothers and I started a side project attempting to recreate one of our old favorites. I researched unity and godot as our two game engines of choice. I like godot because of its open source nature and ease of use. GDScript is a really nice and intuitive language to use.';
        break;
    }
    return detailText;
  }

})(jQuery); // End of use strict